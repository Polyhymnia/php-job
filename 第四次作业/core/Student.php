<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/11/30
 * Time: 10:29
 */

namespace app\core;

class Student
{
    public $sName;
    public $age;
    public $stuId;
    public $gender;
    public $class;





    public function __construct($name,$age,$gender)
    {
        echo '开始创建学生信息';
        $this->sName = $name;
        $this->age=$age;
        $this->gender=$gender;
        $this->stuId = $this::RandStudentId();

    }

/*
 *
 * 分配班级
 */
    public function DistributeClass(){
        echo '分班成功,班号为：'.$this::RandClassNum();
    }
    /*
     * 随机班号
     *
     *
     */
    public function RandClassNum(){
        return rand(1,9);
    }
    /*
    * 随机学号
    *
    */
    public function RandStudentId(){
        $randNum = '';
        for ($i=1;$i<9;$i++){
            $randNum = $randNum .rand(0,9);
        }
        return $randNum;
    }

    private function FamilyMsg(){
        echo '家庭信息创建';
    }

    protected function PersonalMsg(){
        echo '创建个人信息';
    }

    public function __destruct(){
        echo '学生信息创建结束';
    }
}