<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/12/1
 * Time: 16:01
 */

namespace userManage\core;

class user
{
    protected $userId;
    public $userName;
    protected $uEmail;
    public $gender;
    protected $pwd;
    public static $userCount = 0;

    /**
     * user constructor.
     * 进入用户页面
     *+
     */
    public function __construct()
    {
        echo '由用户正在搭建对象<br />';
        self::$userCount++;

    }

    /**
     * 用户注册
     * @param $uName
     * @param $email
     * @param $gender
     * @param $pwd
     */
    public function Register($uName,$email,$gender,$pwd){
        echo '进入注册页面';
        $this->userId = $this->CreateRand();
        $this->userName = $uName;
        $this->uEmail = $email;
        $this->gender = $gender;
        $this->pwd = $pwd;

        echo '恭喜'.$this->userName.'注册成功';
    }

    /**
     * 登录页面
     */
    public function Login(){
        echo '进入登录页面';
    }

    /**
     * 更改密码
     */
    public function ChangePwd(){
        echo '更改密码';
    }

    protected function CreateRand(){

        $num = rand(1000000000,9999999999);
        return $num;
    }

    /**
     * 账号等级
     */
    public function MyLv(){
        echo '<br />这是一个普通用户<br />';
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        echo '<br />用户对象删除<br />';
//        self::$userCount--;

    }

    public function ShowNowOnline(){
        echo  '访问人数：'.self::$userCount.'<br />';
    }
}