<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/12/14
 * Time: 9:17
 */
include_once 'common.php';
$is_login = getLoginStatus();
if(!$is_login){
    echo '尚未登入，<a href="loginUi.php">请登入</a>';
    die;
}else {
    session_start();
    $token = $_COOKIE['Token'];

//清空COOKIES
    setcookie('Token', '', time() - 3600, '/');

//清空SESSION
    unset($_SESSION[$token]);

    echo '退出成功<a href="loginUi.php">请登入</a>';
}