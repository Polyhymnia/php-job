<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/12/14
 * Time: 9:23
 */
//判断是否已登入，true:登入；false:未登入
function getLoginStatus(){
    session_start();
    $Token = $_COOKIE["Token"];
    if(empty($Token)){
        return false;
    }
    if(empty($_SESSION[$Token])){
        return false;
    }
    return true;
}