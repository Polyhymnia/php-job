<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/12/9
 * Time: 8:36
 */

function getLoginStatus(){
    session_start();
    $key = $_COOKIE["userkey"];
    if(isset($_SESSION[$key]["username"])){
        return true;
    }
    return false;
}