<?php

Class DbTools
{
    public static $conn;

    /**
     * 连接数据库
     * @param $sql
     */
    static function DatabaseConnect($conn)
    {
        self::$conn = $conn;
    }

    /**
     * 实现sql查询语句
     * @param $conn
     * @param $sql
     * @return array
     */
    static function Select($sql)
    {
        $result = self::$conn->query($sql);
//   return $result;
        $data = [];
        while (true) {
            $row = $result->fetch_assoc();
            if (empty($row)) {
                break;
            }
//    print_r($row);
//    echo $row['CardNo'];
            $data[] = $row;
        }
        return $data;
    }
}