<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/12/8
 * Time: 14:24
 */

include_once '../common/dbFunction.php';

$username = $_POST['uesrname'];
$psd = $_POST['psd'];

//设置通过对象
$pass = false;

//创建连接数据库对象；
$conn = new mysqli();
//连接数据库
//所需要连接的数据库  '127.0.0.1','root','root','bank'

$conn->connect('127.0.0.1','root','root','bank');
if(!empty($conn->connect_error)){
    echo 'MYSQL数据库连接错误: '.$conn->connect_error;
    die;
}
//$result = $conn->query("select * from bankcard ");
//$result;

//$row = $result->fetch_assoc();
//
//echo $row['CardNo'];

DbTools::DatabaseConnect($conn);//将数据库已连接状态递给DbTools
$data=DbTools::Select("select * from bankcard ");//将查询语句查递给DbTools


for ($i = 0 ; $i<count($data);$i++){
    if ($data[$i]['CardNo']==$username && $data[$i]['CardPwd']==$psd){
       $pass = true;
       $ac_id = $data[$i]['AccountId'];
        break;
    }
}
$newUrl = '';
$msg = '';
session_start();
if($pass){
    //echo '登录成功';
    $keyValue = md5($username.$psd);
    setcookie('userkey',$keyValue,time()+24*3600,'/');
    //header('location:userinfo.php');
    $newUrl = 'userinfo.php';

    $msg = '登录成功';

    $_SESSION[$keyValue]=[
            'username'=>$username,
            'psd'=>$psd,
            'accountId' => $ac_id
    ];

}else{
    //echo '用户名密码错误';
    //header('location:/login.php');
    $msg = '用户名或密码错误';
    $newUrl = '/login.php';
}
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
    <input type="hidden" id="newUrl" value="<?php echo $newUrl;?>">
    <input type="hidden" id="msg" value="<?php echo $msg;?>">
</body>
<script>
    onload = function () {
        var newUrl = document.getElementById('newUrl').value;
        var msg = document.getElementById('msg').value;
        alert(msg);
        location.href = newUrl;
    }
</script>

</html>

