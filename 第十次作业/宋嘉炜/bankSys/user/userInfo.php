<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/12/14
 * Time: 8:41
 */
include_once 'common.php';
include_once '../tools/DbTools.php';
$is_login = getLoginStatus();
if(!$is_login){
    echo '尚未登入，<a href="loginUi.php">请登入</a>';
    die;
}else{
    session_start();
    $usreInfo = $_SESSION[$_COOKIE['Token']];
    $realName = $usreInfo['realName'];
    $cardNum = $usreInfo['cardNum'];
    DbTools::InitDb();
    $sql = "select CardMoney from bankcard where CardId = ".$usreInfo['CardId'];
    $result = DbTools::select($sql);
    //关闭连接
    DbTools::close();
    $money = $result[0]['CardMoney'];
}

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>用户中心</title>
	</head>
	<body>
		<h3>欢迎您，<?php echo $realName;?><a href="logoutAction.php">退出</a></h3>
		<p>卡号：<?php echo $cardNum; ?>,￥<?php echo $money; ?></p>
        <ul>
            <li><a href="../money/moneyIn.php">存钱</a></li>
            <li><a href="../money/moneyOut.php">取钱</a></li>
            <li><a href="../money/transfer.php">转账</a></li>
        </ul>
	</body>
</html>
